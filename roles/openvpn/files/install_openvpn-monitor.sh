#!/bin/bash

PASTA="/opt/openvpn-monitor"
if [ ! -d $PASTA ]; then
  mkdir $PASTA
  cd $PASTA
  virtualenv .
  . bin/activate
  pip install openvpn-monitor gunicorn
fi;
